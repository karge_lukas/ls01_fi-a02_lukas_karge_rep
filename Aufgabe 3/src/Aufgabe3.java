
public class Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.printf("Fahrenheit    |   Celsius");
		System.out.printf("\n-------------------------");
		System.out.printf( "\n%1s%12s%10s", "-20","|","-28.89" ); 
		System.out.printf( "\n%1s%12s%10s","-10","|","-23.33");
		System.out.printf( "\n%3s%12s%10s","+0","|","-17.78");
		System.out.printf( "\n%3s%12s%10s","+20","|","-6.67");
		System.out.printf( "\n%3s%12s%10s","+30","|","-1.11");

	}

}
