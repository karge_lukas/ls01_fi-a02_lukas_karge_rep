﻿import java.util.Scanner; 
	 	 
	 class Fahrkartenautomat { 
		 
	public static void main(String[] args)  { 
			 double rückgabebetrag; 
			 double zuZahlenderBetrag; 
			 
			 char abschluss;
			 Scanner tastatur = new Scanner(System.in);
			 

		do {
		 //double rückgabebetrag; 
		 //double zuZahlenderBetrag; 
		 //char abschluss;
		 zuZahlenderBetrag = fahrkartenbestellungErfassen();
		 rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		 fahrkartenAusgeben(); 
		 rueckgeldAusgeben(rückgabebetrag); 
		 
		 System.out.println("\nVergessen Sie nicht, den  Fahrschein\n"+ 
		 "vor Fahrtantritt entwerten zu  lassen!\n"+ 
		 "Wir wünschen Ihnen eine gute  Fahrt."); 
		 System.out.println("Weiteres Ticket kaufen? [j/n]");
		 abschluss = tastatur.next().charAt(0);
		 } while (abschluss == 'j');
		tastatur.close();
		 }
		
	public static double fahrkartenbestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);
        double ticketpreis;
        int anzahlDerFahrkarten;

        System.out.print("Preis eines Tickets (EURO): ");
        ticketpreis = tastatur.nextDouble();

        System.out.print("Anzahl der Fahrkarten: ");
        anzahlDerFahrkarten = tastatur.nextInt();

        if (!(anzahlDerFahrkarten >= 1 && anzahlDerFahrkarten <= 10)) {
            anzahlDerFahrkarten = 1;
            System.out.println("Anzahl der Karten ungültig. Fortfahren mit einer Karte");
        }

        return (double) anzahlDerFahrkarten * ticketpreis;
    }
	  
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) 
	 { 
	 Scanner tastatur = new Scanner(System.in); 
	 double eingezahlterGesamtbetrag = 0.0; 
	 while(eingezahlterGesamtbetrag < zuZahlenderBetrag) 
		 { 
	 System.out.printf("%s%.2f%s\n","Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " EURO");
	  System.out.printf("%s%.2f%s%.2f%s","Eingabe (mind.  ", 0.05, " Euro, höchstens ", 2.0, " Euro): "); 
	 double eingeworfeneMünze = tastatur.nextDouble();
	  eingezahlterGesamtbetrag += eingeworfeneMünze; 
	 } 
	  
	 return (eingezahlterGesamtbetrag - zuZahlenderBetrag) * 100; 
	 } 
	public static void fahrkartenAusgeben() 
	 { 
	 
	 System.out.println("\nFahrschein wird ausgegeben"); 
	  for (int i = 0; i < 8; i++) 
	 { 
	 System.out.print("="); 
	 try { 
	 Thread.sleep(250); 
	 } catch (InterruptedException e) { 
	 } 
	 } 
	 System.out.println("\n\n"); 
	 } 
	  
	public static void rueckgeldAusgeben(double rückgabebetrag) 
	 { 
	 if(rückgabebetrag > 0.0) 
	 { 
	 System.out.printf("%s%.2f%s\n","Der Rückgabebetrag  in Höhe von ", rückgabebetrag / 100, " EURO"); 
	 System.out.println("wird in folgenden Münzen  ausgezahlt:\n"); 
	  
	 String format = "%.2f%s\n"; 
	 
	 while(rückgabebetrag>= 200) // 2 EURO-Münzen 
		 { 
	 System.out.printf(format, 2.0, " EURO");
	 rückgabebetrag -= 200; 
	 } 
	 while(rückgabebetrag >= 100) // 1 EURO-Münzen 
		 { 
	 System.out.printf(format, 1.0, " EURO");
	  rückgabebetrag -= 100; 
	 } 
	 while(rückgabebetrag >= 50) // 50 CENT-Münzen 
		 { 
	 System.out.printf(format, 0.50, " EURO");
	  rückgabebetrag -= 50; 
	 } 
	 while(rückgabebetrag >= 20) // 20 CENT-Münzen
		 { 
	 System.out.printf(format, 0.20, " EURO");
	  rückgabebetrag -= 20; 
	 } 
	 while(rückgabebetrag >= 10) // 10 CENT-Münzen 
		 { 
	 System.out.printf(format, 0.10, " EURO"); 
	  rückgabebetrag -= 10; 
	 } 
	 while(rückgabebetrag >= 5)// 5 CENT-Münzen 
		 { 
	 System.out.printf(format, 0.05, " EURO"); 
	  rückgabebetrag -= 5; 
	 } 
	 } 
	 } 	  
	
	 
	 
	
	
	 }